<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Calculator</title>
</head>
<body>
<h1><%= "Calculator" %>
</h1>
<form action="${pageContext.request.contextPath}/calc" method="post" style="
    display: grid;
    gap: 1rem;
    width: 600px;
    margin: 0 auto;
">
    <label>
        First number: <br>
        <input type="number" name="num1">
    </label>
    <label>
        Second number: <br>
        <input type="number" name="num2">
    </label>
    <label>
        <select name="operation">
            <option value="+"> +</option>
            <option value="-"> -</option>
            <option value="*"> *</option>
            <option value="/"> /</option>
        </select>
    </label>
    <label>
        <button>OK</button>
    </label>
</form>
</body>
</html>